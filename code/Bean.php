<?php
/**
 * - Add custom setters for database-saved values
 * - Add settable properties that don't save to database
 * 
 * @export(TODO.Bean)
 */

namespace RDB;

class Bean extends \RedBeanPHP\OODBBean {


    /**
     * Wire $bean->prop to Model->&getProp()
     */
    public function &__get($prop){
        $model = $this->box();
        if (!is_null($model)&&method_exists($model,$method='get'.ucfirst($prop)))return $model->$method();

        return parent::__get($prop);
    }

    /**
     * Get bean as an assoc array, with `_type` set, so you can later import that array.
     */
    public function export($meta = false, $parents = false, $onlyMe = false, $filters = []){
        $array = parent::export(...func_get_args());
        $array['_type'] = $this->__info['type'];
        
        return $array;
    }


    /**
     * Get an array of all properties store in Database
     */
    public function props(){
        return $this->properties;
    }

    /**
     * Calls \RDB::store($this)
     */
    public function save(){
        return \RDB::store($this);
    }
}
