# RDB: Redbean PHP Wrapper
Custom property getters on beans, better error reporting, sql-verbs as an API. Additional convenienence functions.


## Getting Started
Install: `composer require taeluf/rdb 0.2.*` or `v0.2.x-dev` or `{"require":{"taeluf/rdb": "0.2.*"}}`

We use `\RDB::` instead of `\R::`. See [Redbean docs](https://redbeanphp.com/index.php?p=/connection) for things not covered here.

### 1. Setup your connection
```php
@import(GettingStarted.setupDb)
```

### 2. Write Some Data
```php
@import(GettingStarted.writeData)
```

### 3. Create a Model (if you need)
[Custom Redbean Models](https://redbeanphp.com/index.php?p=/models) let you add functionality to redbean. RDB simplifies some things.

```php
@import(GettingStarted.Model)
```
& Use that model:
```php
@import(GettingStarted.usingModel)
```





## Other Stuff
- `$bean->props()` to get all properties (just the ones from the database)
- `$bean->export()` adds `_type` to the exported array.
    - (untested) Call `$bean->import($exported)` and your exported array will load into the database, thanks to the `_type` addition
- Table names (types) are made lower-case, and non alpha non-underscore characters are removed, so you don't have to be so careful about the type you pass into `RDB::find()`
 <!-- * - Custom property getters that don't save to database -->
 <!-- * - `$bean->props()` to get all properties (just the ones from the database) -->
 <!-- * - `$bean->export()` adds `_type` to the exported array. -->
 <!-- * - `$bean->save()` -->
 <!-- *     - (untested) Call `$bean->import($exported)` and your exported array will load into the database, thanks to the `_type` addition -->
