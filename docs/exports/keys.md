
# TODO.Bean
- Add custom setters for database-saved values
- Add settable properties that don't save to database


# GettingStarted.setupDb
// \RDB::setup('mysql:host=localhost;dbname=mydatabase', 'user', 'password' );
\RDB::setup('sqlite:'.__DIR__.'/testdb.sqlite');
//empty our recipe table at start of each test
\RDB::delete('recipe',[]);


# GettingStarted.writeData
\RDB::insert('recipe', ['name'=>'Tofu Scramble']);// <- convenience method in RDB
\RDB::insert('recipe', ['name'=>'Vege Stirfry']); 
$loMein = \RDB::insert('recipe', ['name'=>'Lo Mein']);//also returns a bean
$loMein->description = "A non-authentic Chinese noodle dish";
// \RDB::store($loMein); // <- The redbean way
$loMein->save();   // <- Convenience method in RDB
\RDB::update('recipe', ['id'=>$loMein->id,'name'=>'Veggie Lo Mein']); //<-- Convenience method in RDB. Updates based on the id
\RDB::update('recipe', ['name'=>'Veggie Stirfry'], ['name'=>'Vege Stirfry']); //<-- 2nd array is used as WHERE params for the update 

$list = \RDB::select('recipe', ['name'=>'Veggie Lo Mein']);
$loadedLoMein = $list[0];

$list2 = \RDB::select('recipe', ['name'=>'Veggie Stirfry']);
$loadedStirfry = $list2[0];


# GettingStarted.usingModel
$tofu  = \RDB::select('recipe', ['name'=>'Tofu Scramble'])[0];
$slug = $tofu->slug; // <- not stored in db
$url = '/recipe/'.$slug.'/';

return true
    && $this->compare('tofu-scramble', $slug)
;


# GettingStarted.Model
// RDBModel namespace is required
namespace RDBModel;

class Recipe extends \RedBeanPHP\SimpleModel {

    //Custom property getters MUST start with &get
    public function &getSlug(){
        $name = strtolower($this->name);
        $parts = explode(' ', $name);
        $slug = implode('-', $parts);
        return $slug;
    }
}

