<?php

//@export_start(GettingStarted.Model)

// RDBModel namespace is required
namespace RDBModel;

class Recipe extends \RedBeanPHP\SimpleModel {

    //Custom property getters MUST start with &get
    public function &getSlug(){
        $name = strtolower($this->name);
        $parts = explode(' ', $name);
        $slug = implode('-', $parts);
        return $slug;
    }
}
//@export_end(GettingStarted.Model)
